F90 ?= xlf2003

D=-WF,-D
I=-I
M=-I
L=-L

F90FLAGS += -g -O0 -WF,-qfpp -C

FPPFLAGS = $DSTRINGIFY_SIMPLE $DIBM
CPPFLAGS = -WF,-DSTRINGIFY_SIMPLE,-DIBM

ifeq ($(PFUNIT_ABI),64)
  FPPFLAGS += $DLONG_PTR
  CPPFLAGS += -DLONG_PTR
endif
